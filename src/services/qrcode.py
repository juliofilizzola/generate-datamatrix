import os
import treepoem
from src.dto import item
from upload import upload_img
from src.config import config


def create_qr_code_datamatrix(data: item.Item):

    image = treepoem.generate_barcode(
        barcode_type='datamatrix',  # One of the supported codes.
        data=data.dataQrCode,
        options={
            "version": "32x32",
            "border": "0"
        }
    )

    img_name = f"{data.name}_datamatrix.png"

    path_img = f"src/img/datamatrix/{img_name}"

    path_aws = f"orion_tags/datamatrix/{img_name}"

    image.convert('1').save(path_img)

    img_s3 = upload_img.upload_file(
        path_img,
        f'{config.env()["BUCKET_IMAGES"]}-{config.env()["ENVIRONMENT"]}',
        object_name=path_aws
    )

    os.remove(path_img)

    return f'https://{config.env()["BUCKET_IMAGES"]}-{config.env()["ENVIRONMENT"]}.s3.amazonaws.com/{path_aws}'


def create_qr_code_bar_code(data: item.Item):
    image = treepoem.generate_barcode(
        barcode_type='code128',  # One of the supported codes.
        data=data.dataQrCode,
    )

    img_name = f"{data.name}_barcode.png"

    path_img = f"src/img/barcode/{img_name}"

    path_aws = f"orion_tags/barcode/{img_name}"

    image.convert('1').save(path_img)

    img_s3 = upload_img.upload_file(
        path_img,
        f'{config.env()["BUCKET_IMAGES"]}-{config.env()["ENVIRONMENT"]}',
        object_name=path_aws
    )

    os.remove(path_img)

    return f'https://{config.env()["BUCKET_IMAGES"]}-{config.env()["ENVIRONMENT"]}.s3.amazonaws.com/{path_aws}'


