import os
import shutil
import subprocess

from dotenv import dotenv_values

config = dotenv_values(".env")

# import os


def env():
    return {
        'AWS_KEY': config['AWS_KEY'],
        'AWS_SECRET': config['AWS_SECRET'],
        'AWS_REGION_APP': config['AWS_REGION_APP'],
        'ENVIRONMENT': config['ENVIRONMENT'],
        'BUCKET_IMAGES': config['BUCKET_IMAGES']
    }


def gs():
    if shutil.which("gs") is None:
        print("GhostScrip is not available, search for it")
        try:
            gs = subprocess.Popen(["which", "gs"], stdout=subprocess.PIPE)
            gs_path = gs.stdout.read()
            gs_path = gs_path.decode() if isinstance(gs_path, bytes) else gs_path
            print("GhostScrip found in", gs_path)
            os.environ["PATH"] += ":" + os.path.dirname(gs_path)
        except Exception as e:
            raise Warning("GhostScrip not found")
