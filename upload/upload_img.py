import logging
import boto3
import os
from botocore.exceptions import ClientError
from src.config import config


def upload_file(file_name, bucket, object_name=None):
    """Upload a file to an S3 bucket

    :param file_name: File to upload
    :param bucket: Bucket to upload to
    :param object_name: S3 object name. If not specified then file_name is used
    :return: True if file was uploaded, else False
    """

    # If S3 object_name was not specified, use file_name
    if object_name is None:
        object_name = os.path.basename(f"orion/{file_name}")

    # Upload the file
    s3_client = boto3.client(
        's3',
        aws_access_key_id=config.env()['AWS_KEY'],
        aws_secret_access_key=config.env()['AWS_SECRET'],
    )
    try:
        response = s3_client.upload_file(
            file_name, bucket, object_name,
            ExtraArgs={'ContentType': 'image/png'})
    except ClientError as e:
        logging.error(e)
        return False
    return True
