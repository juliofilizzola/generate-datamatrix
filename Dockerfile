FROM python

WORKDIR /app

COPY ./requirements.txt .

RUN pip install -r requirements.txt

COPY . .

EXPOSE 8000

ENTRYPOINT ["sh", ".docker/entrypoint.sh"]

RUN ["chmod", "+x", ".docker/entrypoint.sh"]
