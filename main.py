from fastapi import FastAPI
from src.services import qrcode
from src.dto import item
from mangum import Mangum
from src.config import config

config.gs()


app = FastAPI(title='Serverless Lambda FastAPI')


@app.post("/datamatrix")
async def create_qr_code_datamatrix(data: item.Item):
    return qrcode.create_qr_code_datamatrix(data)


@app.post("/barcode")
async def create_qr_code_barcode(data: item.Item):
    return qrcode.create_qr_code_bar_code(data)


handler = Mangum(app=app, lifespan="off")

